#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"

// Network Topology
//
//       10.1.1.0
// n0 -------------- n1
//    point-to-point
//
// Default Network Topology
//
//       10.1.2.0
// n2 -------------- n3
//    point-to-point
//

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("FirstAssignment");

int
main(int argc, char* argv[])
{
    CommandLine cmd(__FILE__);
    cmd.Parse(argc, argv);

    Time::SetResolution(Time::NS);
    LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
    LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);

    NodeContainer nodes1;
    nodes1.Create(2);

    NodeContainer nodes2;
    nodes2.Create(2);

    PointToPointHelper pointToPoint;
    pointToPoint.SetDeviceAttribute("DataRate", StringValue("10Mbps"));
    pointToPoint.SetChannelAttribute("Delay", StringValue("4ms"));

    NetDeviceContainer devices1, devices2;
    devices1 = pointToPoint.Install(nodes1.Get(0), nodes1.Get(1)); //devono essere esattamente due nodi
    devices2 = pointToPoint.Install(nodes2.Get(0), nodes2.Get(1)); //devono essere esattamente due nodi

    InternetStackHelper stack;
    stack.Install(nodes1);
    stack.Install(nodes2);

    Ipv4AddressHelper address1, address2;
    address1.SetBase("10.1.1.0", "255.255.255.0");
    address2.SetBase("10.1.2.0", "255.255.255.0");

    Ipv4InterfaceContainer interfaces1 = address1.Assign(devices1);
    Ipv4InterfaceContainer interfaces2 = address2.Assign(devices2);

    UdpEchoServerHelper echoServer1(9);
    UdpEchoServerHelper echoServer2(12);

    ApplicationContainer serverApps1 = echoServer1.Install(nodes1.Get(1));
    serverApps1.Start(Seconds(1.0));
    serverApps1.Stop(Seconds(10.0));

    UdpEchoClientHelper echoClient1(interfaces1.GetAddress(1), 9);
    echoClient1.SetAttribute("MaxPackets", UintegerValue(1));
    echoClient1.SetAttribute("Interval", TimeValue(Seconds(1.0)));
    echoClient1.SetAttribute("PacketSize", UintegerValue(1024));

    ApplicationContainer clientApps1 = echoClient1.Install(nodes1.Get(0));
    clientApps1.Start(Seconds(2.0));
    clientApps1.Stop(Seconds(10.0));


    ApplicationContainer serverApps2 = echoServer2.Install(nodes2.Get(1));
    serverApps2.Start(Seconds(3.0));
    serverApps2.Stop(Seconds(10.0));

    UdpEchoClientHelper echoClient2(interfaces2.GetAddress(1), 12);
    echoClient2.SetAttribute("MaxPackets", UintegerValue(2));
    echoClient2.SetAttribute("Interval", TimeValue(Seconds(1.5)));
    echoClient2.SetAttribute("PacketSize", UintegerValue(2048));

    ApplicationContainer clientApps2 = echoClient2.Install(nodes2.Get(0));
    clientApps2.Start(Seconds(5.0));
    clientApps2.Stop(Seconds(10.0));

    pointToPoint.EnablePcap("firstAssignmentPCAP", devices2.Get(0), true);
    pointToPoint.EnablePcap("firstAssignmentPCAP", devices2.Get(1), true);

    Simulator::Run();
    Simulator::Destroy();
    return 0;
}
